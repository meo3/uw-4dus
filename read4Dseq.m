%{
MIT License

Copyright (c) 2016 Miles Olsen

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
%}


function [voxel_data_4d, md] = read4Dseq(seq_file_path, varargin)
%READ4DUS Read 4D ultrasound data based on .seq file metadata.
%
%   Arguments:
%     seq_file_path = The path to the .seq file which is output by the
%         volume conversion software.
%             e.g. '50828948.seq'
%         Or a fully qualified path, if for instance the .seq file is
%         not in Matlab's current directory:
%             e.g. 'F:/converted-us-vols/phantom/50828948.seq'
%
%   Optional arguments:
%     d_frames = an array containing the frame numbers of all desired
%         frames that you wish to import. Allows you to reduce memory use
%         by importing a limited data set which contains only the frame(s)
%         where something relevant appears.
%             e.g. [3, 4, 5, 6, 7]    or, equivalently    3:7
%         Also could be used to select every other frame if reduced
%         temporal resolution is acceptable
%             e.g. 1:2:26
%
%   Returns:
%     4D matrix of voxel data.
%     (Optionally, returns image metadata if called with two outputs)

    % Check that number of arguments falls within the
    narginchk(1, 2);

    % Read .seq file
    [seq_text, md] = read_seq_file(seq_file_path);

    % Check user input and set the desired frames if not specified
    d_frames = validate_desired_frames(varargin, md.Nt);

    % Allocate 4D matrix with appropriate dimensions for storing voxel data
    v_size = [md.Nx, md.Ny, md.Nz, length(d_frames)];
    voxel_data_4d = zeros(v_size, 'uint8');

    % Get path where .img files live (assuming they're with the .seq file)
    [pathstr, ~, ~] = fileparts(seq_file_path);

    % We expect Nx*Ny*Nz elements to be read in from each file
    expected_len = prod(v_size(1:3));

    % Get just the file names in the .seq file (i.e. lines [2, Nt+1])
    file_names = seq_text(2:md.Nt+1, 1);

%     fprintf('###file_names = \n');
%     disp(file_names);
    h_waitbar = waitbar(0, 'Reading 4D sequence');
    finishup_obj = onCleanup(@() close(h_waitbar));

    % Read in contents of desired .img files
    for fnum = 1:length(d_frames)
        % Assemble full filename string
%         fprintf('###d_frames(fnum) = %d\n', d_frames(fnum));
        fname = file_names{d_frames(fnum),1};
%         fprintf('###fname = %s\n', fname);
        full_name = strcat(pathstr, filesep, fname, '.img');

        esc_fname = strrep(fname, '_', '\_'); % Escape underscores for display
        waitbar(fnum/length(d_frames), h_waitbar, ...
                sprintf('Reading file: %s', esc_fname));
        data_1d = read_img_file(full_name, expected_len);

        % Resize 1D data based on the spatial components of vsize
        voxel_data_4d(:,:,:,fnum) = reshape(data_1d, v_size(1:3));
    end
end




function d_frames = validate_desired_frames(main_varargin, Nt)
    % Ensure that the list of frames to output contains reasonable frame numbers
    if isempty(main_varargin)
        d_frames = 1:1:Nt;
        return;
    end

    d_frames = main_varargin{1};

    if isempty(d_frames)
        error('Argument d_frames is empty');
    end

    if ~isrow(d_frames)
        error('Argument d_frames is not a row vector');
    end

    if (min(d_frames) < 1) || (Nt < max(d_frames))
        fprintf('Warning: desired_frames contains value outside [1,%d]\n', Nt);

        % Clamp frame numbers that are less than 1 or greater than Nt
        d_frames = d_frames(1 <= d_frames & d_frames <= Nt);
    end

    % Eliminate any duplicate frame numbers that might be in here.
    %d_frames = unique(d_frames); % This sorts the output

    % Using Jan Simon's approach, as described by Loren Shure here:
    % http://blogs.mathworks.com/loren/2009/11/26/unique-values-without-rearrangement/
    [d_frames_sorted, sorted_indices] = sort(d_frames(:));
    unique_values(sorted_indices) = ([1; diff(d_frames_sorted)] ~= 0);
    d_frames = d_frames(unique_values);
end



function [seq_text, metadata] = read_seq_file(seq_file_path)
    % Open and read .seq file to get image metadata
    fid = fopen(seq_file_path);
    seq_text = textscan(fid, '%s', 'Delimiter','\n');
    fclose(fid);

    % The Nx1 cell array which contains the lines of the text file as
    % character vectors is the {1,1} element of textscan's output
    seq_text = seq_text{1,1};

    % Read first line to get the number of frames in this 4D sequence
    N_frames = sscanf(seq_text{1,1}, '%d');


    % Read the last line to get voxel dimensions (in mm)
    voxel_dims = sscanf(seq_text{end, 1}, 'VRES %f %f %f');

    % Read (last-1) line for 3D matrix dimensions (in voxels)
    v_size = sscanf(seq_text{end-1, 1}, 'VSIZE %d %d %d');
    v_size = [v_size', N_frames];

    % Read (last-2) line for the time spent acquiring each frame (in ms)
    delta_t = sscanf(seq_text{end-2, 1}, 'TIME_FRAME %f');


    % Populate values of metadata struct
    metadata = struct('Nx', v_size(1), ...
                      'Ny', v_size(2), ...
                      'Nz', v_size(3), ...
                      'Nt', v_size(4), ...
                      'Dx', voxel_dims(1), ...
                      'Dy', voxel_dims(2), ...
                      'Dz', voxel_dims(3), ...
                      'Dt', delta_t);
end



function data_1d = read_img_file(fname, expected_len)
    % Read binary data from img file to get voxel intensities

    %fprintf('Reading file: %s\n', fname);

    fid = fopen(fname);
    if fid == -1
        fprintf('Error opening file:\n%s\n', fname);
        error('Unable to open file');
    end

    data_1d = uint8(fread(fid));
    fclose(fid);

    % Check that we read in the expected amount of data
    if (length(data_1d) ~= expected_len)
        fprintf('Error reading file:\n%s\n', fname);
        error('Expected %d values, but read in %d from file', ...
                expected_len, length(data_1d));
    end
end
