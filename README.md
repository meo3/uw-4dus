# README #

Download these .m files and place them in a folder that is on your Matlab path.


e.g. on Windows, the default Matlab directory might be

    C:\Users\%username%\Documents\Matlab\

### What is this repository for? ###

* Loading 4D ultrasound datasets in Matlab
* Visualizing 4D datasets in Matlab